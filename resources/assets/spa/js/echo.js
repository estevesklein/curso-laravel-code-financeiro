import Echo from 'laravel-echo';
import JwtToken from './services/jwt-token';

window.Pusher = require('pusher-js');

window.Echo = new Echo({
   broadcaster: 'pusher',
   key: 'e1c27bc5dd3225b04c13'
});

const changeJwtTokenInEcho = value => {
   window.Echo.connector.pusher.config.auth.headers['Authorization'] =
       JwtToken.getAuthorizationHeader();
};

JwtToken.event('updateToken', value => {
   changeJwtTokenInEcho(value);
});

changeJwtTokenInEcho(JwtToken.token);